extern crate wayland_client;
use wayland_client::*;
use wayland_client::protocol::*;

extern crate ash;

extern crate wayland_protocols;

pub fn run() {
    let (display, mut queue) = wayland_client::default_connect()
        .unwrap();

    let registry = display.get_registry();

    use wayland_protocols::xdg_shell::client;

    wayland_env!(WaylandEnv,
                 compositor: wl_compositor::WlCompositor
                 xdgwmb: client::XdgWmBase);
    let env_token = EnvHandler::<WaylandEnv>::init(&mut queue, &registry);
    queue.sync_roundtrip().unwrap();

    let state = queue.state();
    let env = state.get(&env_token);
    let compositor = &env.compositor;
    let xdgwmb = &env.xdgwmb;

    let surface = compositor.create_surface();

    let xdg_surface = xdgwmb.get_xdg_surface(&surface)
        .expect("Could not get xdg surface");

    let topl = xdg_surface.get_toplevel()
        .expect("Could not get xdg toplevel");

    fn ping_handler(evqh: &mut EventQueueHandle,
                    data: &mut ID,
                    xdgwmb: &XdgWmBase,
                    serial: u32)
    {
        xdgwmb.pong(serial);
    }

    let implem = client::Implementation<()> {
        ping: ping_handler,
    };

    queue.register(&my_object, implem, &mut ());
}
